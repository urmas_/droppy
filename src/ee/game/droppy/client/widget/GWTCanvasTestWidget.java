package ee.game.droppy.client.widget;

import org.vaadin.gwtgraphics.client.shape.Circle;
import org.vaadin.gwtgraphics.client.shape.Rectangle;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GWTCanvasTestWidget extends Composite implements Updateable{
	private static final int COORDINATE_STEP = 2;
	private static final int RADIUS_CHANGE = 1;
	/**
	 * This is the entry point method.
	 */
	private Circle circle;
	private Rectangle rectangle;
	private boolean movingLeft;
	private boolean expanding;
	private Canvas canvas;
	private String command;
	private static final String BALL_COMMAND = "balloffury";
	private static final String SQUARE_COMMAND = "rectangleofdeath";
	private static final int RECT_CENTERX = 125;
	private static final int RECT_CENTERY = 125;
	public GWTCanvasTestWidget() {
		canvas = Canvas.createIfSupported();
		canvas.setWidth(600+"px");
		canvas.setHeight(600+"px");
		Button button = new Button("The Ball of Fury");
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				command = BALL_COMMAND;
				circle = new Circle(50, 50, 30);
				circle.setFillColor("red");
			}
		});
		Button button2 = new Button("The Rectangle of Death");
		button2.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				command = SQUARE_COMMAND;
				rectangle = new Rectangle(RECT_CENTERX-25,RECT_CENTERY-25,50,50);
				rectangle.setFillColor("blue");
			}
		});
		VerticalPanel mainPanel = new VerticalPanel();
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(button);
		buttonPanel.add(button2);
		mainPanel.add(buttonPanel);
		mainPanel.add(canvas);
		initWidget(mainPanel);
	}
	@Override
	public void update(){
		final Context2d drawBoard = canvas.getContext2d();
		drawBoard.clearRect(0, 0, 600, 600);
		if (BALL_COMMAND.equals(command)) {
			updateBallOfFury();
			//render
			drawBoard.setFillStyle(circle.getFillColor());
			drawBoard.arc(circle.getX(), circle.getY(), circle.getRadius(), 0, Math.PI*2);
			drawBoard.fillRect(0, 0, 50, 50);
		}else if (SQUARE_COMMAND.equals(command)) {
			updateSquareOfDeath();
			//render
			drawBoard.setFillStyle(rectangle.getFillColor());
			drawBoard.fillRect(rectangle.getX(), rectangle.getY(), rectangle.getWidth(), rectangle.getHeight());
			System.out.println("balls2");
		}
	}
	private void updateSquareOfDeath()
	{
		int width = rectangle.getWidth();
		int height =  rectangle.getHeight();
		if(expanding){
			width += RADIUS_CHANGE;
			height += RADIUS_CHANGE;
			if(width > 200)
			{
				expanding = false;
			}
		}else{
			width -= RADIUS_CHANGE;
			height -= RADIUS_CHANGE;
			if(width < 20)
			{
				expanding = true;
			}
		}
		int x = RECT_CENTERX-(rectangle.getWidth()/2);
		int y = RECT_CENTERY-(rectangle.getHeight()/2);
		rectangle.setWidth(width);
		rectangle.setHeight(height);
		rectangle.setX(x);
		rectangle.setY(y);
	}
	private void updateBallOfFury()
	{
		int x = circle.getX();
		int y = circle.getY();
		int radius = circle.getRadius();
		if (movingLeft) {
			x -= COORDINATE_STEP;
			y -= COORDINATE_STEP;
			radius -= RADIUS_CHANGE;
			if (x < 50) {
				movingLeft = false;
			}
		} else {
			x += COORDINATE_STEP;
			y += COORDINATE_STEP;
			radius += RADIUS_CHANGE;
			if (x > 300) {
				movingLeft = true;
			}
		}
		circle.setX(x);
		circle.setY(y);
		circle.setRadius(radius);
	}
}
