package ee.game.droppy.client.widget;

import org.vaadin.gwtgraphics.client.DrawingArea;
import org.vaadin.gwtgraphics.client.shape.Circle;
import org.vaadin.gwtgraphics.client.shape.Rectangle;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GWTGraphicsTestWidget extends Composite implements Updateable{
	private static final int COORDINATE_STEP = 2;
	private static final int RADIUS_CHANGE = 1;
	/**
	 * This is the entry point method.
	 */
	private Circle circle;
	private Rectangle rectangle;
	private boolean movingLeft;
	private boolean expanding;
	private DrawingArea canvas;
	private String command;
	private static final String BALL_COMMAND = "balloffury";
	private static final String SQUARE_COMMAND = "rectangleofdeath";
	private static final int RECT_CENTERX = 225;
	private static final int RECT_CENTERY = 225;
	public GWTGraphicsTestWidget() {
		canvas = new DrawingArea(600,600);
		Button button = new Button("The Ball of Fury");
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				command = BALL_COMMAND;
				circle = new Circle(50, 50, 30);
				circle.setFillColor("red");
				canvas.clear();
				canvas.add(circle);
			}
		});
		Button button2 = new Button("The Rectangle of Death");
		button2.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				command = SQUARE_COMMAND;
				rectangle = new Rectangle(RECT_CENTERX-25,RECT_CENTERY-25,50,50);
				rectangle.setFillColor("blue");
				canvas.clear();
				canvas.add(rectangle);
			}
		});
		VerticalPanel mainPanel = new VerticalPanel();
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(button);
		buttonPanel.add(button2);
		
		mainPanel.add(buttonPanel);
		mainPanel.add(canvas);
		
		initWidget(mainPanel);
	}
	@Override
	public void update(){
		if (BALL_COMMAND.equals(command)) {
			updateBallOfFury();
			
		}else if (SQUARE_COMMAND.equals(command)) {
			updateSquareOfDeath();
		}
	}
	private void updateSquareOfDeath()
	{
		int width = rectangle.getWidth();
		int height =  rectangle.getHeight();
		if(expanding){
			width += RADIUS_CHANGE;
			height += RADIUS_CHANGE;
			if(width > 200)
			{
				expanding = false;
			}
		}else{
			width -= RADIUS_CHANGE;
			height -= RADIUS_CHANGE;
			if(width < 20)
			{
				expanding = true;
			}
		}
		int x = RECT_CENTERX-(rectangle.getWidth()/2);
		int y = RECT_CENTERY-(rectangle.getHeight()/2);
		rectangle.setWidth(width);
		rectangle.setHeight(height);
		rectangle.setX(x);
		rectangle.setY(y);
	}
	private void updateBallOfFury()
	{
		int x = circle.getX();
		int y = circle.getY();
		int radius = circle.getRadius();
		if (movingLeft) {
			x -= COORDINATE_STEP;
			y -= COORDINATE_STEP;
			radius -= RADIUS_CHANGE;
			if (x < 50) {
				movingLeft = false;
			}
		} else {
			x += COORDINATE_STEP;
			y += COORDINATE_STEP;
			radius += RADIUS_CHANGE;
			if (x > 300) {
				movingLeft = true;
			}
		}
		circle.setX(x);
		circle.setY(y);
		circle.setRadius(radius);
	}
}
