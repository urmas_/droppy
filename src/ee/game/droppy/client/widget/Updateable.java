package ee.game.droppy.client.widget;

public interface Updateable {
	void update();
}
