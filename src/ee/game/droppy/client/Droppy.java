package ee.game.droppy.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.RootPanel;

//import ee.game.droppy.client.widget.GWTCanvasTestWidget;
import ee.game.droppy.client.widget.GWTGraphicsTestWidget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Droppy implements EntryPoint {

	private Timer timer;
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final GWTGraphicsTestWidget gwtGraphicsModuleTest = new GWTGraphicsTestWidget();
//		final GWTCanvasTestWidget gwtCanvasTestWidget = new GWTCanvasTestWidget();
		timer = new Timer() {
			@Override
			public void run() {
				gwtGraphicsModuleTest.update();
//				gwtCanvasTestWidget.update();
			}
		};
		timer.scheduleRepeating(10);
		RootPanel.get("droppyCanvas").add(gwtGraphicsModuleTest);
//		RootPanel.get("droppyCanvas").add(gwtCanvasTestWidget);
	}
	
}
