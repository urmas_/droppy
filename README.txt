## Setting up project ##
1. Download:
	* Eclipse 3.7+ // From web
	* Google Plugin for Eclipse // Eclipse Marketplace (within Eclipse)
	* EGit // Eclipse Marketplace (within Eclipse)
2. Checkout from Git repository.
3. You are ready!

## Module notes ##
gwt-graphics-1.0.0.jar - http://code.google.com/p/gwt-graphics/
-The goal of the GWT Graphics library is to provide a consistent cross-browser vector graphics library for GWT